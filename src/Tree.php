<?php

declare(strict_types=1);

namespace assist;


/**
 * Class Tree
 * @package assist
 */
class Tree
{
    /**
     * @var object 对象实例
     */
    protected static $instance;

    /**
     * 配置参数
     * @var array
     */
    protected static $config = [
        'id' => 'id',
        'pid' => 'pid',
        'title' => 'title',
        'child' => 'child',
        'html' => '┝ ',
        'step' => 4,
    ];

    /**
     * 架构函数
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        self::$config = array_merge(self::$config, $config);
    }

    /**
     * 配置参数
     * @param array $config
     * @return self
     */
    public static function config(array $config = []): self
    {
        if (is_null(self::$instance)) {
            self::$instance = new static( array_merge(self::$config, $config));
        }
        return self::$instance;
    }

    /**
     * 将数据集格式化成层次结构
     * @param array/object $lists 要格式化的数据集，可以是数组，也可以是对象
     * @param int $pid 父级id
     * @param int $max_level 最多返回多少层，0为不限制
     * @param int $curr_level 当前层数
     * @return array
     */
    public static function toLayer(array $lists = [], int $pid = 0, int $max_level = 0, int $curr_level = 0): array
    {
        $trees = [];
        $lists = array_values($lists);
        foreach ($lists as $key => $value) {
            if ($value[static::$config['pid']] == $pid) {
                if ($max_level > 0 && $curr_level == $max_level) {
                    return $trees;
                }
                unset($lists[$key]);
                $child = self::toLayer($lists, $value[self::$config['id']], $max_level, $curr_level + 1);
                if (!empty($child)) {
                    $value[self::$config['child']] = $child;
                }
                $trees[] = $value;
            }
        }
        return $trees;
    }

    /**
     * 将数据集格式化成列表结构
     * @param array|object $lists 要格式化的数据集，可以是数组，也可以是对象
     * @param integer $pid 父级id
     * @param integer $level 级别
     * @return array 列表结构(一维数组)
     */
    public static function toList(array $lists = [], int $pid = 0, int $level = 0): array
    {
        if (is_array($lists)) {
            $trees = [];
            foreach ($lists as $key => $value) {
                if ($value[self::$config['pid']] == $pid) {
                    $title_prefix = str_repeat("&nbsp;", $level * self::$config['step']) . self::$config['html'];
                    $value['level'] = $level + 1;
                    $value['title_prefix'] = $level == 0 ? '' : $title_prefix;
                    $value['title_display'] = $level == 0 ? $value[self::$config['title']] : $title_prefix . $value[self::$config['title']];
                    $trees[] = $value;
                    unset($lists[$key]);
                    $trees = array_merge($trees, self::toList($lists, $value[self::$config['id']], $level + 1));
                }
            }
            return $trees;
        } else {
            foreach ($lists as $key => $value) {
                if ($value[self::$config['pid']] == $pid && is_object($value)) {
                    $title_prefix = str_repeat("&nbsp;", $level * self::$config['step']) . self::$config['html'];
                    $value['level'] = $level + 1;
                    $value['title_prefix'] = $level == 0 ? '' : $title_prefix;
                    $value['title_display'] = $level == 0 ? $value[self::$config['title']] : $title_prefix . $value[self::$config['title']];
                    $lists->offsetUnset($key);
                    $lists[] = $value;
                    self::toList($lists, $value[self::$config['id']], $level + 1);
                }
            }
            return $lists;
        }
    }

    /**
     * 根据子节点返回所有父节点
     * @param array $lists 数据集
     * @param string $id 子节点id
     * @return array
     */
    public static function getParents(array $lists = [], $id = ''): array
    {
        $trees = [];
        foreach ($lists as $value) {
            if ($value[self::$config['id']] == $id) {
                $trees[] = $value;
                $trees = array_merge(self::getParents($lists, $value[self::$config['pid']]), $trees);
            }
        }
        return $trees;
    }

    /**
     * 获取所有子节点id
     * @param array $lists 数据集
     * @param string $pid 父级id
     * @return array
     */
    public static function getChildsId(array $lists = [], $pid = ''): array
    {
        $result = [];
        foreach ($lists as $value) {
            if ($value[self::$config['pid']] == $pid) {
                $result[] = $value[self::$config['id']];
                $result = array_merge($result, self::getChildsId($lists, $value[self::$config['id']]));
            }
        }
        return $result;
    }

    /**
     * 获取所有子节点
     * @param array $lists 数据集
     * @param string $pid 父级id
     * @return array
     */
    public static function getChilds(array $lists = [], $pid = ''): array
    {
        $result = [];
        foreach ($lists as $value) {
            if ($value[self::$config['pid']] == $pid) {
                $result[] = $value;
                $result = array_merge($result, self::getChilds($lists, $value[self::$config['id']]));
            }
        }
        return $result;
    }
}
<?php declare(strict_types=1);

namespace assist;

class Time
{


    /**
     * 返回今年某一天开始和结束的时间戳
     *
     * @param int $days
     * @param bool $opreate
     * @return array
     */
    public static function someDayTime(int $days = 0,bool $opreate=true): array
    {

        $symbol=$opreate?'-':'+';

        list($y, $m, $d) = explode('-', date('Y-m-d',strtotime("$symbol$days days")));

        $y  = (int)$y;
        $m  = (int)$m;
        $d  = (int)$d;

        $begin  = mktime(0, 0, 0, $m, $d, $y);
        $end    = mktime(23, 59, 59, $m, $d, $y);

        return [$begin, $end];
    }
    /**
     * 返回某一年开始和结束的时间戳
     *
     * @param int $year
     * @return array
     */
    public static function someYear(int $year=0): array
    {
        $y  = (int)date('Y')-$year;
        $begin  = mktime(0, 0, 0, 1, 1, $y);
        $end    = mktime(23, 59, 59, 12, cal_days_in_month(CAL_GREGORIAN,12,$y), $y);
        return [$begin, $end];
    }


    /**
     * 返回本周开始和结束的时间戳
     *
     * @return array
     */
    public static function week(): array
    {
        list($y, $m, $d, $w) = explode('-', date('Y-m-d-w'));
        $y=(int)$y;
        $m=(int)$m;
        $d=(int)$d;
        $w=(int)$w;
        if ($w == 0){
            $w = 7; //修正周日的问题
        }
        $begin = mktime(0, 0, 0, $m, $d - $w + 1, $y);
        $end   = mktime(23, 59, 59, $m, $d - $w + 7, $y);
        return [$begin, $end];
    }

    /**
     * 返回上周开始和结束的时间戳
     *
     * @return array
     */
    public static function lastWeek(): array
    {
        $timestamp = time();
        $begin  = strtotime(date('Y-m-d', strtotime("last week Monday", $timestamp)));
        $end    = strtotime(date('Y-m-d', strtotime("last week Sunday", $timestamp))) + 24 * 3600 - 1;
        return [$begin, $end];
    }

    /**
     * 返回本月开始和结束的时间戳
     *
     * @return array
     */
    public static function month(): array
    {
        list($y, $m, $t) = explode('-', date('Y-m-t'));
        $y=(int)$y;
        $m=(int)$m;
        $t=(int)$t;
        $begin  = mktime(0, 0, 0, $m, 1, $y);
        $end    = mktime(23, 59, 59, $m, $t, $y);
        return [$begin, $end];
    }

    /**
     * 返回上个月开始和结束的时间戳
     *
     * @return array
     */
    public static function lastMonth(): array
    {
        $y  = date('Y');
        $m  = date('m');
        $y  = (int)$y;
        $m  = (int)$m;
        $begin  = mktime(0, 0, 0, $m - 1, 1, $y);
        $end    = mktime(23, 59, 59, $m - 1, date('t', $begin), $y);

        return [$begin, $end];
    }



    /**
     * 获取几天前零点到现在/昨日结束的时间戳
     *
     * @param int $day 天数
     * @param bool $now 返回现在或者昨天结束时间戳
     * @return array
     */
    public static function dayToNow(int $day = 1, bool $now = true): array
    {
        $end = time();
        if (!$now) {
            list($foo, $end) = self::someDayTime(1);
        }
        $begin = mktime(0, 0, 0, (int)date('m'), (int)date('d') - $day,(int) date('Y'));
        return [$begin, $end];
    }

    /**
     * 返回几天前的时间戳
     *
     * @param int $day
     * @return int
     */
    public static function daysAgo(int $day = 1): int
    {
        $nowTime = time();
        return $nowTime - self::daysToSecond($day);
    }

    /**
     * 返回几天后的时间戳
     *
     * @param int $day
     * @return int
     */
    public static function daysAfter(int $day = 1): int
    {
        $nowTime = time();
        return $nowTime + self::daysToSecond($day);
    }

    /**
     * 天数转换成秒数
     *
     * @param int $day
     * @return int
     */
    public static function daysToSecond(int $day = 1): int
    {
        return $day * 86400;
    }

    /**
     * 周数转换成秒数
     *
     * @param int $week
     * @return int
     */
    public static function weekToSecond(int $week = 1): int
    {
        return self::daysToSecond() * 7 * $week;
    }



    /**
     * 计算本月最大连续天数
     *
     * @param array $times
     * @return int
     */
    public static function continueDays(array $times = []):int
    {

        rsort($times);
        $length = count($times);
        $continueDays = 1;
        $continueDaysArray = [];
        for ($i = 0; $i < $length; $i++) {

            if ($i != $length - 1) {

                if ($times[$i] - $times[$i + 1] == self::daysToSecond()) {
                    $continueDays += 1;
                } else {
                    $continueDaysArray[] = $continueDays;
                    $continueDays = 1;
                }
            } else {
                $continueDaysArray[] = $continueDays;
            }
        }

        return count($continueDaysArray) > 0 ? max($continueDaysArray) : 0;
    }

    /**
     * 获取一周具体日期
     *
     * @param int $time
     * @param string $format
     * @return array
     */
    public static function getWeeks(int $time = null, string $format = 'm.d'): array
    {

        $time = isset($time) ? $time : time();
        //获取当前周几
        $week = date('w', $time) == 0 ? 7 : date('w', $time);

        $date = [];
        for ($i = 1; $i <= 7; $i++) {
            $date[$i] = date($format, strtotime('+' . $i - $week . ' days', $time));
        }

        return $date;
    }
}

<?php

declare(strict_types=1);

namespace assist\encrypt;
/**
 * Class Bcrypt
 * @package assist\encrypt
 */
class Bcrypt extends Encrypt
{


    /**
     * 加密
     * @param string|null $value
     * @return string|bool
     */
    public function make(string $value = null)
    {

        $hash = empty($value) ? false : password_hash($value, PASSWORD_BCRYPT, ['cost' => $this->cost]);

        return $hash;
    }

    /**
     * 解密是否对等
     * @param string $value
     * @param string $hashedValue
     * @return bool
     */
    public function check(string $value = null, string $hashedValue = null): bool
    {
        if (empty($value) || empty($hashedValue)) {
            return false;
        }
        return strlen($hashedValue) === 0 ? false : password_verify($value, $hashedValue);
    }


}